# nand2tetris_projects

coursera [Build a Modern Computer from First Principles: From Nand to Tetris (Project-Centered Course)](https://www.coursera.org/learn/build-a-computer/home/welcome)

[The official website of Nand to Tetris courses](https://www.nand2tetris.org/)

The course homework consists of a series 6 projects:

* Project 1: Building elementary logic gates like And, Or, Not, Multiplexor, and more
* Project 2: Building a family of adder chips, culminating in the construction of an Arithmetic Logic Unit (ALU)
* Project 3: Building registers and memory units, culminating in the construction of a Random Access Memory (RAM)
* Project 4: Learning a machine language and using it to write some illustrative low-level programs
* Project 5: Using the chipset built in projects 1-3 to build a Central Processing Unit (CPU) and a hardware platform capable of executing programs written in the machine language introduced in project 4
* Project 6: Developing an assembler, i.e. a capability to translate programs written in symbolic machine language into binary, executable code.
